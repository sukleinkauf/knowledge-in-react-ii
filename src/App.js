import React, { Component } from 'react';
import './App.css';
import Person from  './Person/Person'

class App extends Component {
  state = {
    persons: [
      { name: 'Max', age: 28 },
      { name: 'Manu', age: 28 },
      { name: 'Suelem', age: 28 }
    ]
  };


  switchNameHandler = (newName) => {
    this.setState({
      persons: [
        { name: newName, age: 28 },
        { name: 'Manu', age: 28 },
        { name: 'Suelem', age: 2 }
      ]
    })
  };

  render()  {
    return (
      <div className="App">
        <h1>Hello World</h1>
        <button onClick={this.switchNameHandler.bind(this,'Vini!!')}>Change Names</button>
        <Person 
          name={this.state.persons[0].name} 
          age={this.state.persons[0].age} />
        <Person 
          name={this.state.persons[1].name} 
          age={this.state.persons[1].age}
          click={this.switchNameHandler.bind(this,'Vinicius')}> Testando Props Children </Person> 
        <Person 
          name={this.state.persons[2].name} 
          age={this.state.persons[2].age}/>
      </div>
    );
  }
}

export default App;
